# TASK MANAGER
Training project

[SCREENSHOTS](https://drive.google.com/drive/folders/10pugq5YjNiBLUye9sGcjt8Jpm0YbRdmz?usp=sharing)

## TECH STACK
JAVA CORE

SPRING FRAMEWORK

## HARDWARE
CPU: i7

RAM: 16G

SSD: 512GB

## SOFTWARE
System: Mojove 10.14.6

Version JDK: 1.8.0_282

## PROGRAM RUN
```bash
java -jar ./taskmanager.jar
```

## DEVELOPER INFO
name: Alexey Pogasiy

e-mail: apogasiy@tsconsulting.com